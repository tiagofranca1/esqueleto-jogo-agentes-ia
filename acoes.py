from enum import Enum
from dataclasses import dataclass

class AcoesJogador(Enum):
    MOVER_CIMA = 'cima',
    MOVER_BAIXO = 'baixo',
    MOVER_ESQUERDA = 'esquerda',
    MOVER_DIREITA = 'direita',
    MOVIMENTO_INVALIDO = 'movimento_invalido'

@dataclass
class AcaoJogador():
    tipo: str
    parametros: tuple = tuple()

    @classmethod
    def mover_cima(cls, x, y):
        return cls(AcoesJogador.MOVER_CIMA, (x, y))
    
    @classmethod
    def mover_baixo(cls, x, y):
        return cls(AcoesJogador.MOVER_BAIXO, (x, y))
    
    @classmethod
    def mover_esquerda(cls, x, y):
        return cls(AcoesJogador.MOVER_ESQUERDA, (x, y))
    
    @classmethod
    def mover_direita(cls, x, y):
        return cls(AcoesJogador.MOVER_DIREITA, (x, y))
    
    @classmethod
    def movimento_invalido(cls):
        return cls(AcoesJogador.MOVIMENTO_INVALIDO)