from agentes.abstrato import AgenteAbstrato
class AgentePrepostoESHumano(AgenteAbstrato):
    
    def adquirirPercepcao(self, percepcao_mundo):
        """ Inspeciona a disposicao dos elementos no objeto de visao e escreve
        na tela para o usuário saber o que seu agente está percebendo.
        """

        for linha in percepcao_mundo.tabuleiro:
            line = ''
            for pino in linha:
                line += str(pino)
            line = line.replace('0', ' O ').replace('1', ' X ').replace('2', '   ')
            print(line)
    
    def escolherProximaAcao(self):
        from acoes import AcaoJogador
        acao, x, y = (s for s in input("Insira a ação (ação, x, y): ").split(',', 3))
        if acao == 'mover_cima':
            return AcaoJogador.mover_cima(int(x), int(y))
        elif acao == 'mover_baixo':
            return AcaoJogador.mover_baixo(int(x), int(y))
        elif acao == 'mover_esquerda':
            return AcaoJogador.mover_esquerda(int(x), int(y))
        elif acao == 'mover_direita':
            return AcaoJogador.mover_direita(int(x), int(y))
        else:
            return AcaoJogador.movimento_invalido()