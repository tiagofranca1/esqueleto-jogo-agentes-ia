from enum import Enum, auto
from acoes import AcoesJogador
from regras_jogo.regras_abstratas import AbstractRegrasJogo

class AgentesRestaUm(Enum):
    JOGADOR_PADRAO = auto()

class RestaUm(AbstractRegrasJogo):

    def __init__(self):
        self.tabuleiro = [[2, 2, 1, 1, 1, 2, 2],
                        [2, 2, 1, 1, 1, 2, 2],
                        [1, 1, 1, 1, 1, 1, 1],
                        [1, 1, 1, 0, 1, 1, 1],
                        [1, 1, 1, 1, 1, 1, 1],
                        [2, 2, 1, 1, 1, 2, 2],
                        [2, 2, 1, 1, 1, 2, 2]]
        self.dim_max = 6
        self.dim_min = 0
        self.distancia_salto = 2
        self.contagem_pinos = 32
    
    def registrarAgentePersonagem(self, personagem):
        return 1

    def isFim(self):
        return self.contagem_pinos == 1

    def gerarCampoVisao(self, id_agente):
        from percepcoes import PercepcoesJogador
        return PercepcoesJogador(self.tabuleiro)

    def registrarProximaAcao(self, id_agente, acao):
        self.acao_jogador = acao

    def atualizarEstado(self, diferencial_tempo):
        from acoes import AcoesJogador
        if self.is_acao_valida():
            x, y = self.acao_jogador.parametros
            if self.acao_jogador.tipo  == AcoesJogador.MOVER_CIMA:
                y_alvo = y - self.distancia_salto
                self.tabuleiro[y][x] = 0
                self.tabuleiro[y-1][x] = 0
                self.tabuleiro[y_alvo][x] = 1
            elif self.acao_jogador.tipo == AcoesJogador.MOVER_BAIXO:
                y_alvo = y + self.distancia_salto
                self.tabuleiro[y][x] = 0
                self.tabuleiro[y+1][x] = 0
                self.tabuleiro[y_alvo][x] = 1
            elif self.acao_jogador.tipo == AcoesJogador.MOVER_ESQUERDA:
                x_alvo = x - self.distancia_salto
                self.tabuleiro[y][x] = 0
                self.tabuleiro[y][x-1] = 0
                self.tabuleiro[y][x_alvo] = 1
            elif self.acao_jogador.tipo == AcoesJogador.MOVER_DIREITA:
                x_alvo = x + self.distancia_salto
                self.tabuleiro[y][x] = 0
                self.tabuleiro[y][x+1] = 0
                self.tabuleiro[y][x_alvo] = 1
            self.contagem_pinos -= 1
            print('Pinos restantes: ' + str(self.contagem_pinos))
        else:
            print('Ação inválida.')

    def terminarJogo(self):
        print('Restou um!')
    
    def is_acao_valida(self):
        if self.acao_jogador.tipo == AcoesJogador.MOVIMENTO_INVALIDO:
            return False

        x, y = self.acao_jogador.parametros

        if self.tabuleiro[y][x] == 1:
            if self.acao_jogador.tipo  == AcoesJogador.MOVER_CIMA:
                y_alvo = y - self.distancia_salto
                if y_alvo < self.dim_min:
                    return False
                elif self.tabuleiro[y_alvo][x] == 2 or self.tabuleiro[y_alvo][x] == 1:
                    return False
            elif self.acao_jogador.tipo == AcoesJogador.MOVER_BAIXO:
                y_alvo = y + self.distancia_salto
                if y_alvo  > self.dim_max:
                    return False
                elif self.tabuleiro[y_alvo][x] == 2 or self.tabuleiro[y_alvo][x] == 1:
                    return False
            elif self.acao_jogador.tipo == AcoesJogador.MOVER_ESQUERDA:
                x_alvo = x - self.distancia_salto
                if x_alvo < self.dim_min:
                    return False
                elif self.tabuleiro[y][x_alvo] == 2 or self.tabuleiro[y][x_alvo] == 1:
                    return False
            elif self.acao_jogador.tipo == AcoesJogador.MOVER_DIREITA:
                x_alvo = x + self.distancia_salto
                if x_alvo > self.dim_max:
                    return False
                elif self.tabuleiro[y][x_alvo] == 2 or self.tabuleiro[y][x_alvo] == 1:
                    return False
            
            return True
        else:
            return False
